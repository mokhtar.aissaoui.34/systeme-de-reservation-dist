# Compilation : 

```bash
cd build
make
```


# Execution : 

```bash
cd build
./serveur [port]
./client [ip serveur][port serveur]  #ip serveur : 0.0.0.0
```


# Suppression des fichiers binaires et des executables : 

```bash
make fclean
```


# ATTENTION !

## Après la connexion on demande d'entrer le nom de l'utilisateur 

## En cas de problème l'hors de l'exécution, penser à utiliser la commande :

```bash
ipcrm -a
```

## Penser à déconnecter tous les  clients avant de quitter
